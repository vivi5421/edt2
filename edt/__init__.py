import argparse
import yaml
import sys
import traceback
from bom import *
from filters import *

def get_options(subparsers):
    parser = subparsers.add_parser("run")
    parser.set_defaults(action=run)
    parser.add_argument("-i", "--interactive", action="store_true")
    parser.add_argument("--recursion_limit", type=int, default=100000)
    parser.add_argument("config", type=lambda config: Config.load(config))

def get_or(llist, index, default=None):
    return llist[index] if index < len(llist) else default

def process(args, solutions, current=[0]):
    progress = sum([get_or(current, i, 0)*100/args.config.nb_activities**(i+1) for i in range(args.config.expected_activities)])
    print("Process... ({})".format(progress), end="\t")
    #print("Process...\n{}".format("\n".join([str(activity) for activity in activities])), end="\t")
    if current[-1] == args.config.nb_activities:
        print("UPDATE")
        process(args, solutions, current[:-2] + [current[-2]+1])
        print("Process... {}".format(current), end="\t")
    activities = [args.config.activities[i] for i in current]
    filters_executed = [filter(args.config, activities)._filter()
                        for filter in filters()]
    if all([filter.result for filter in filters_executed]):
        print("SOLUTION")
        print("Solution:\n{}".format(activities))
        interactive_break(args)
        solutions.append(activities)
    else:
        if any([not filter.result for filter in filters_executed if filter.is_blocking]):
            print("BLOCK")
            process(args, solutions, current[:-1] + [current[-1]+1])
        else:
            print("KO")
            process(args, solutions, current + [current[-1]+1])
    print("OVER")

def interactive_break(args):
    if args.interactive:
        input("<Press Enter to continue...>")

def run(args):
    try:
        sys.setrecursionlimit(args.recursion_limit)
        print("Generated {} activities".format(args.config.nb_activities))
        print("Expecting {} activities to generate this schedule".format(args.config.expected_activities))
        interactive_break(args)
        solutions = []
        process(args, solutions)

        print("solutions")
        print(solutions)
    except:
        traceback.print_exc()

