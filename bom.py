import lazy_property
import yaml
from tkinter import Button, Entry, Frame, Label, X, Y, BOTH, LEFT, RIGHT, StringVar
from keyvalue import KeyValue
import os

class _Item(yaml.YAMLObject):

    def _display(self, parent):
        return {key: KeyValue(parent, "{}:".format(key), getattr(self, key)) for key in self.__dict__.keys() if not key.startswith("_")}

    def __str__(self):
        return "{}({})".format(self.__class__.__name__, ", ".join(["{}={}".format(attr, getattr(self, attr)) for attr in self.__dict__]))

    def __repr__(self):
        return self.__str__()

    def __lt__(self, other):
        return hash(self) < hash(other)

    
# CONFIG
class Config(_Item):
    yaml_tag = "!Config"
    
    def __init__(self, teachers, schedules, classrooms, filters):
        self.teachers = teachers
        self.schedules = schedules
        self.classrooms = classrooms
        self.filters = filters

    @lazy_property.LazyProperty
    def activities(self):
      activities = Activities()
      for classroom in sorted(self.classrooms):
          for teacher in sorted(self.teachers):
              for schedule in sorted(self.schedules):
                  activities.append(Activity(classroom, teacher, schedule))
      return activities            

    @lazy_property.LazyProperty
    def expected_nb_activities(self):
        return sum(classroom.class_type.sessions_per_week for classroom in self.classrooms)

    def save(self, config_file):
        with open(os.path.join(config_file), "w") as f:
            f.write(yaml.dump(self))

    @classmethod
    def load(cls, config_file):
        with open(config_file) as f:
            return yaml.load(f, Loader=yaml.Loader)


class ConfigFull(Config):
    yaml_tag = "!ConfigFull"

    def __init__(self, teachers, schedules, classrooms, filters, sports, installations, cycles):
        super().__init__(teachers, schedules, classrooms, filters)
        self.sports = sports
        self.installations = installations
        self.cycles = cycles

    @lazy_property.LazyProperty
    def activities(self):
      activities = Activities()
      for classroom in sorted(self.classrooms):
          for teacher in sorted(self.teachers):
              for schedule in sorted(self.schedules):
                  for sport in sorted(self.sports):
                      for installation in sorted(self.installations):
                          for cycle in sorted(self.cycles):
                              activities.append(ActivityFull(classroom, teacher, schedule, sport, installation, cycle))
      return activities    

    @lazy_property.LazyProperty
    def expected_nb_activities(self):
        return sum(classroom.class_type.sessions_per_week for classroom in self.classrooms) * len(self.cycles)


# ACTIVITIES
class Activities(list):

    @lazy_property.LazyProperty
    def size(self):
        return len(self)


# ACTIVITY
class Activity(_Item):

  def __init__(self, classroom, teacher, schedule):
      self.teacher = teacher
      self.classroom = classroom
      self.schedule = schedule


class ActivityFull(Activity):

  def __init__(self, classroom, teacher, schedule, sport, installation, cycle):
      super().__init__(classroom, teacher, schedule)
      self.sport = sport
      self.installation = installation
      self.cycle = cycle


# TEACHER
class Teacher(_Item):
    yaml_tag = "!Teacher"
    
    def __init__(self, name, min_hours, max_hours):
        self.name = name
        self.min_hours = min_hours
        self.max_hours = max_hours

    @classmethod
    def default(cls):
        return Teacher("new_teacher", 17, 17)

# SCHEDULE
class Schedule(_Item):
    yaml_tag = "!Schedule"

    HOURS = {
      "monday": 0 * 24,
      "tuesday": 1 * 24,
      "wednesday": 2 * 24,
      "thursday": 3 * 24,
      "friday": 4 * 24
    }

    def __init__(self, day, begin, end):
        self.day = day
        self.begin = int(begin)
        self._begin = Schedule.HOURS[day] + self.begin
        self.end = int(end)
        self._end = Schedule.HOURS[day] + self.end
        

    def time_between(self, schedule):
      """
      >>> Schedule("monday", 8, 10).compareTo(Schedule("tuesday", 10, 12))
      24
      >>> Schedule("tuesday", 10, 12).compareTo(Schedule("monday", 8, 10))
      24
      """
      return min([
        abs(schedule._begin - self._end),
        abs(self._begin - schedule._end)
        ])
            
    @classmethod
    def default(cls):
        return Schedule("monday", 8, 10)

# TIME_SCHEDULE
class TimeSchedule(_Item):
    yaml_tag = "!TimeSchedule"

    def __init__(self, weekA, weekB):
        self.weekA = weekA
        self.weekB = weekB


# CLASSTYPE
class ClassType(_Item):
    yaml_tag = "!ClassType"

    def __init__(self, name, level, hours, sessions):
        self.name = name
        self.level = level
        self.hours = hours
        self.sessions = sessions

    @classmethod
    def default(cls):
        return ClassType("new-classtype", 6, [[2, 2]], 2)

# CLASSROOM
class ClassRoom(_Item):
    yaml_tag = "!ClassRoom"

    def __init__(self, class_type, index):
        self.class_type = class_type
        self.index = index

    @classmethod
    def default(cls):
        return ClassRoom("new_classtype", 1)

# SPORT
class Sport(_Item):
    yaml_tag = "!Sport"

    def __init__(self, name, cp):
        self.name = name
        self.cp = cp
    
    @classmethod
    def default(cls):
        return Sport("new_sport", 1)

# INSTALLATION
class Installation(_Item):
    yaml_tag = "!Installation"

    def __init__(self, name, capacity, sports):
        self.name = name
        self.capacity = capacity
        self.sports = sports
    
    @classmethod
    def default(cls):
        return Installation("new_installation", 1, ["sport1"])

# CYCLE
class Cycle(_Item):
    yaml_tag = "!Cycle"

    def __init__(self, ids):
        self.ids = ids

    @classmethod
    def default(cls):
        return Cycle([1])