from tkinter import StringVar, BOTH, LEFT
from tkinter.ttk import Frame, Label, Entry

class KeyValue(Frame):

    def __init__(self, parent, key, default):
        super().__init__(parent)
        self.pack(fill=BOTH)
        Label(self, text=key).pack(side=LEFT)
        self.entry = StringVar(value=default)
        Entry(self, textvariable=self.entry).pack(side=LEFT)
    
    @property
    def value(self):
        return self.entry