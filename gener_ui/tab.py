from functools import partial
from tkinter import ttk, Button, messagebox
from gener_ui.toggledframe import ToggledFrame

class Tab(object):
  """
  Object used to ease creation of a tkinter tab for _Item objects.
  This requires:
    - parent: the container
    - tab_name: title of the tab
    - type: type of object of type _Item to be considered
    - llist: the list of objects to be displayed
    - title_function: the method to give the title of an item
    - default_new: default Item for item creation option
  """

  pos = 1

  CREATE = "create"

  def __init__(self, parent, tab_name, ttype, llist, title_function, refresh):
    self.parent = parent
    self.tab_name = tab_name
    self.ttype = ttype
    self.llist = llist
    self.title_function = title_function
    self.default_new = ttype.default()
    self.refresh = refresh
    self._feed()

  def _feed(self, pos=None):
    self.frame = ttk.Frame(self.parent)
    # To know in which position we want to create this tab. Default is simply add (at the end), but in case we want to refresh the display after a change, we do not want to hvae this tab moved to the last position when it was not the case before.
    if not pos == None:
      self.parent.insert("end" if len(self.parent.tabs()) == pos else pos, self.frame, text=self.tab_name)
      self.parent.select(pos)
    else:
      self.parent.add(self.frame, text=self.tab_name)
    # /position
    self.id = 0
    [self._item(item, self.title_function(item)) for item in self.llist]
    self._item(self.default_new, self.CREATE)

  def _item(self, item, title):
    t = ToggledFrame(self.frame, text=title, relief="raised", borderwidth=1)
    t.pack(fill="x", expand=0, anchor="n")
    new = item._display(t.sub_frame)
    if title == self.CREATE:
      Button(t.sub_frame, text="create", command=partial(self._create, **new)).pack()
    else:
      Button(t.sub_frame, text="delete", command=partial(self._delete, self.id)).pack(side="left")
      Button(t.sub_frame, text="update", command=partial(self._update, self.id)).pack(side="left")
    self.id += 1
    return t
  
  def _delete(self, id):
    t = self.llist[id]
    self.llist.remove(t)
    self._refresh()
  
  def _create(self, **kwargs):
    new = self.ttype(*[kwargs[x].get() for x in kwargs])
    self.llist.append(new)
    self._refresh()

  def _update(self, id):
    t = self.llist[id]
    messagebox.showinfo("Update", t)
    self._refresh()

  def _refresh(self):
    self.refresh()
    if self.frame is not None:
      pos = self.parent.index(self.frame)
      self.frame.destroy()
    self._feed(pos)
