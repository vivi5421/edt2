import os
import shutil
import tkinter as tk
from tkinter import ttk, messagebox, X, Y, BOTH, Button, messagebox
from bom import *
from functools import partial
from gener_ui.tab import Tab

def get_options(subparsers):
    parser = subparsers.add_parser("gener-ui")
    parser.add_argument("--folder", default="config")
    parser.add_argument("--input")
    parser.add_argument("ouput")
    parser.set_defaults(action=run)

class Root(object):

    def __init__(self, config):
        self.root = tk.Tk()
        self.root.title("Gener Schedule")
        self.root.geometry("300x300")
        self.tabs = []
        self.tab_parent = ttk.Notebook(self.root)
        self.get_tabs(config)
        self.tab_parent.pack(expand=1, fill=BOTH)
        self.root.mainloop()

    def get_tabs(self, config):
        self._add_tab("teacher", Teacher, config.teachers, lambda teacher: teacher.name)
        self._add_tab("schedules", Schedule, config.schedules, lambda schedule: "{} {}-{}".format(schedule.day, schedule.begin, schedule.end))
        self._add_tab("classtypes", ClassType, config.classtypes, lambda classType: classType.name)
        self._add_tab("classRooms", ClassRoom, config.classrooms, lambda classRoom: "{}-{}".format(classRoom.classType.name, classRoom.index))
        self._add_tab("sports", Sport, config.sports, lambda sport: sport.name)
        self._add_tab("installations", Installation, config.installations, lambda installation: installation.name)
        self._add_tab("cycles", Cycle, config.cycles, lambda cycle: cycle.ids)

    def _add_tab(self, tab_title, ttype, llist, item_title_method):
        self.tabs.append(Tab(self.tab_parent, tab_title, ttype, llist, item_title_method, _refresh))
    
    def _refresh(self):
        self.tab_parent = ttk.Notebook(self.root)

def run(args):
    if args.input:
        config = ConfigFull.load(os.path.join(args.folder, args.input))
        Root(config)
