import argparse
import functools
import os
import yaml

from colorama import Fore, Back, Style

from bom import *
from filters import *

def _request(name, default, options=None, type=str):
  if options:
    text = "/".join(["{}".format("{}{}{}".format(Fore.RED, str(x), Style.RESET_ALL) if x == default else x) for x in options])
  else:
    text = default if default else ""
  res = input("{}? ({})".format(name, text))
  return default if res == "" and default else type(res)

def another(type):
  return _request("Another {}".format(type), "n", ["y", "n"]).lower() == "y"

def get_options(subparsers):
    parser = subparsers.add_parser("gener")
    parser.add_argument("--folder", default="config")
    parser.add_argument("ouput")
    parser.add_argument("--full", action="store_true")
    parser.set_defaults(action=run)

class _Gener(object):

  def __init__(self, auto=True):
    self.cpt = 0
    self.auto = auto

  def gener(self):
    self.cpt += 1
    default_id = "id{}".format(self.cpt)
    return {default_id if self.auto else _request("id", default_id): self._gener(self.cpt)}

  def _gener(self, i):
    pass


class GenerTeacher(_Gener):
  def _gener(self, i):
    return Teacher(
        _request("name", "teacher{}".format(i)),
        _request("min hours", 17, type=int),
        _request("max hours", 17, type=int)
    )

class GenerTimeSchedule(_Gener):
  def get_schedule(self, week):
    schedule = []
    while another("week {}".format(week)):
      schedule.append(_request("hours", 2, type=int))
    return schedule

  def get_schedules(self):
    schedules = []
    while another("time schedule"):
      schedules.append({"a": self.get_schedule("a"), "b": self.get_schedule("b")})
    return schedules

  def _gener(self, i):
    return TimeSchedule(
      self.get_schedules()
    )

class GenerClassType(_Gener):
  def get_weeks(self):
    hours_per_week = []
    while another("hours/week scheduling"):
        hours_per_week.append([_request("hours/week A", 2, type=int), _request("hours/week B", 2, type=int)])
    return hours_per_week

  def _gener(self, i):
    return ClassType(
        _request("level", 6, [6, 5, 4, 3], int),
        self.get_weeks(),
        _request("sessions per week", 1, type=int)
    )


class GenerClassRoom(_Gener):
  def __init__(self, class_types, auto=True):
    super().__init__(auto)
    self.class_types = class_types

  def _gener(self, i):
    return ClassRoom(
        self.class_types[_request("ClassType", list(self.class_types.keys())[0], self.class_types.keys())],
        _request("index", i, type=int)
    )


class GenerSport(_Gener):
  def _gener(self, i):
    return Sport(
      _request("name", "sport{}".format(i)),
      _request("cp", 1, [1, 2, 3, 4], int)
    )


class GenerInstallation(_Gener):
  def __init__(self, sports, auto=True):
    super().__init__(auto)
    self.sports = sports

  def get_sports(self):
    sports = []
    while another("sport"):
        sports.append(self.sports[_request("id", self.sports.keys()[0], self.sports.keys())])
    return sports

  def _gener(self, i):
    return Installation(
        _request("name", "install{}".format(i)),
        _request("capacity", 1, [0.5, 1, 2], float),
        self.get_sports()
    )


class GenerSchedule(_Gener):
  def _gener(self, i):
    schedules = []
    for day in ["monday", "tuesday", "wednesday", "thursday", "friday"]:
      for creneau in [[8, 10], [10, 12], [13, 15], [15, 17], [8, 11], [14, 17]]:
        if not day == "wednesday" or creneau[0] < 12:
          schedules.append(Schedule(day, creneau[0], creneau[1]))
    return schedules


class GenerCycle(_Gener):
  def _gener(self, i):
    return Cycle(
      _request("id", "1", ["1", "2", "3", "4", "12", "34"], type=list)
    )
  
def gener_filters():
  return Filter.__subclasses__()




def dump(data):
  print(yaml.dump(data))

def gener_list(name, generator):
  llist = []
  while another(name):
    for _, v in generator.gener().items():
      llist.append(v)
    dump(llist)
  return llist

def gener_dict(name, generator):
  ddict = {}
  while another(name):
    for k, v in generator.gener().items():
      ddict[k] = v
    dump(ddict)
  return ddict


def run(args):
  teachers = gener_list("teacher", GenerTeacher())
  print(GenerSchedule().gener().items())
  for k, v in GenerSchedule().gener().items():
    schedules = v
  dump(schedules)
  class_types = gener_dict("class_type", GenerClassType(auto=False))
  classrooms = gener_list("classroom", GenerClassRoom(class_types))
  filters = gener_filters()
  dump(filters)
  if not args.full:
    config = Config(teachers, schedules, classrooms, filters)
  else:
    sports = gener_dict("sports", GenerSport(auto=False))
    installations = gener_list("installation", GenerInstallation(sports))
    cycles = gener_list("cycle", GenerCycle())
    config = ConfigFull(teachers, schedules, classrooms, filters, sports, installations, cycles)
    config.save(os.path.join(args.folder, "{}.yml".format(args.ouput)))

# # CURRENT ISSUES
# => when creating classroom, request classtype then request to create many classrooms with this classtype