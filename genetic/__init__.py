"""
Simple genetic algorithm guessing a string.
"""
 
# ----- Dependencies
 
from random import random
from string import ascii_letters
import yaml
from filters import *
from bom import *

FILTERS = filters()

def get_options(subparsers):
    parser = subparsers.add_parser("genetic")
    parser.add_argument("config", type=lambda config: Config.load(config))
    parser.add_argument("--chance_to_mutate", help="the chance for an individual to mutate (range 0-1)", default=0.1)
    parser.add_argument("--graded_retain_percent", help="the percent of top-grated individuals to be retained for the next generation (range 0-1)", default=0.2)
    parser.add_argument("--chance_retain_nongrated", help="the chance for a non top-grated individual to be retained for the next generation (range 0-1)", default=0.5)
    parser.add_argument("--population_count", help="Number of individual in the population", default=100)
    parser.add_argument("--generation_count_max", help="Maximum number of generation before stopping the script", default=100000)
    parser.set_defaults(action=run)
    

# One-linner for randomly choose a element in an array
# This one-linner is fastest than random.choice(x).
choice = lambda x: x[int(random() * len(x))]
 
 
# ----- Do not touch anything after this line
 
# Number of top-grated individuals to be retained for the next generation
def graded_individual_retain_count(args):
    return int(args.population_count * args.graded_retain_percent)

# Precompute the length of the expected string (individual are always fixed size objects)
def length_of_expected(args):
    return args.config.expected_nb_activities

def middle_length_of_expected(args):
    return length_of_expected(args) // 2

# Maximum fitness value
def maximum_fitness(args):
    return len(args.config.filters)
 
# ----- Genetic Algorithm code
# Note: An individual is simply an array of LENGTH_OF_EXPECTED_STR characters.
# And a population is nothing more than an array of individuals.
 
def get_random_activity(args):
    """ Return a random char from the allowed charmap. """
    return choice(args.config.activities)
     
 
def get_random_individual(args):
    """ Create a new random individual. """
    return [get_random_activity(args) for _ in range(args.config.expected_nb_activities)]
 
 
def get_random_population(args):
    """ Create a new random population, made of `POPULATION_COUNT` individual. """
    return [get_random_individual(args) for _ in range(args.population_count)]
 
 
def get_individual_fitness(args, individual):
    """ Compute the fitness of the given individual. """
    fitness = 0
    for ffilter in args.config.filters:
        ff = ffilter(args.config, individual)
        ff.filter()
        if ff.result:
            fitness += 1
    return fitness
 
 
def average_population_grade(args, population):
    """ Return the average fitness of all individual in the population. """
    total = 0
    for individual in population:
        total += get_individual_fitness(args, individual)
    return total / args.population_count
 
 
def grade_population(args, population):
    """ Grade the population. Return a list of tuple (individual, fitness) sorted from most graded to less graded. """
    graded_individual = []
    for individual in population:
        graded_individual.append((individual, get_individual_fitness(args, individual)))
    return sorted(graded_individual, key=lambda x: x[1], reverse=True)
 
 
def evolve_population(args, population):
    """ Make the given population evolving to his next generation. """
 
    # Get individual sorted by grade (top first), the average grade and the solution (if any)
    raw_graded_population = grade_population(args, population)
    average_grade = 0
    solution = []
    graded_population = []
    for individual, fitness in raw_graded_population:
        average_grade += fitness
        graded_population.append(individual)
        if fitness == maximum_fitness(args):
            solution.append(individual)
    average_grade /= args.population_count
 
    # End the script when solution is found
    if solution:
        return population, average_grade, solution    
 
    # Filter the top graded individuals
    parents = graded_population[:graded_individual_retain_count(args)]
 
    # Randomly add other individuals to promote genetic diversity
    for individual in graded_population[graded_individual_retain_count(args):]:
        if random() < args.chance_retain_nongrated:
            parents.append(individual)
 
    # Mutate some individuals
    for individual in parents:
        if random() < args.chance_to_mutate:
            place_to_modify = int(random() * length_of_expected(args))
            individual[place_to_modify] = get_random_activity(args)
 
    # Crossover parents to create children
    parents_len = len(parents)
    desired_len = args.population_count - parents_len
    children = []
    while len(children) < desired_len:
        father = choice(parents)
        mother = choice(parents)
        if True: #father != mother:
            #child = [act for act in father if act.classroom.class_type.level in [6, 5]] + [act for act in mother if act.classroom.class_type.level in [4, 3]]
            child = father[:middle_length_of_expected(args)] + mother[middle_length_of_expected(args):]
            children.append(child)
 
    # The next generation is ready
    parents.extend(children)
    return parents, average_grade, solution
 
 
# ----- Runtime code
 
def run(args):
    """ Main function. """
 
    # Create a population and compute starting grade
    population = get_random_population(args)
    average_grade = average_population_grade(args, population)
    print('Starting grade: %.2f' % average_grade, '/ %d' % maximum_fitness(args))
 
    # Make the population evolve
    i = 0
    solution = None
    log_avg = []
    while not solution and i < args.generation_count_max:
        population, average_grade, solution = evolve_population(args, population)
        if i & 255 == 255:
            print('Current grade: %.2f' % average_grade, '/ %d' % maximum_fitness(args), '(%d generation)' % i)
        if i & 31 == 31:
            log_avg.append(average_grade)
        i += 1
 
    import pygal
    line_chart = pygal.Line(show_dots=False, show_legend=False)
    line_chart.title = 'Fitness evolution'
    line_chart.x_title = 'Generations'
    line_chart.y_title = 'Fitness'
    line_chart.add('Fitness', log_avg)
    line_chart.render_to_file('bar_chart.svg')
     
    # Print the final stats
    average_grade = average_population_grade(args, population)
    print('Final grade: %.2f' % average_grade, '/ %d' % maximum_fitness(args))
 
    # Print the solution
    if solution:
        print('Solution found (%d times) after %d generations.' % (len(solution), i))
        for i in range(len(solution)):
            print("#"*5)
            print("solution {}".format(i+1))
            print("\n".join([str(act) for act in solution[i]]))
            print("#"*5)
    else:
        print('No solution found after %d generations.' % i)
        print('- Last population was:')
        for number, individual in enumerate(population):
            print(number, '->',  ''.join(individual))
 
 
if __name__ == '__main__':
    run(None)