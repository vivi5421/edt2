First draft of tool to generate schedules for Sport teachers.
This is configured by JSON file.

Unfortunately, today the tool cannot work correctly because of too many options to be tested.

3 solutions:
- split the program in a 2 steps problem (first try to get schedules for each classroom/teacher, then try to configure sports and rooms)
- improve the genetic algorhtym
- Use a Constraint Solving solution (http://www.choco-solver.org/ for example)