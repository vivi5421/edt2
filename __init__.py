import argparse
import colorama

from bom import *
import gener
import gener_ui
import edt
import genetic

OPTIONS = [gener, gener_ui, edt, genetic]

def get_options():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()
    [option.get_options(subparsers) for option in OPTIONS]
    return parser

def run():
    args = get_options().parse_args()
    print(args)
    assert hasattr(args, "action"), "No action defined"
    colorama.init(autoreset=True)
    args.action(args)

if __name__ == "__main__":
    run()