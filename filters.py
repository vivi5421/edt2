import itertools
import lazy_property


_filters = None
def filters():
    global _filters
    if _filters is None:
        _filters = Filter.__subclasses__()
    return _filters

class Filter(object):

    is_blocking = False

    def __init__(self, config, activities):
        self.config = config
        self.activities = activities

    def filter(self):
        self.result = self._filter()
        return self

    def _filter(self):
        return False

    def _group_by(self, key):
        return itertools.groupby(sorted(self.activities, key=key), key=key)

    @lazy_property.LazyProperty
    def group_by_teachers(self):
        return self._group_by(lambda activity: activity.teacher)

    @lazy_property.LazyProperty
    def group_by_class(self):
        return self._group_by(lambda activity: activity.classroom)

    @lazy_property.LazyProperty
    def required_activities(self):
        return sum(classroom.class_type.sessions_per_week for classroom in self.config.classrooms)

class MaxHoursTeachers(Filter):

    is_blocking = True

    def _filter(self):
        for teacher, act in self.group_by_teachers: 
            if sum([activity.schedule.end - activity.schedule.begin for activity in act]) > int(teacher.max_hours):
                return False
        return True

class MinHoursTeachers(Filter):

    def _filter(self):
        for teacher, act in self.group_by_teachers: 
            if sum([activity.schedule.end - activity.schedule.begin for activity in act]) < int(teacher.min_hours):
                return False
        return True

class Min24HoursBetweenSession(Filter):

    is_blocking = True

    def _filter(self):
        for classroom, act in self.group_by_class:
            for combination in itertools.combinations(act, 2):
                if combination[0].schedule.time_between(combination[1].schedule) < 24:
                    return False
        return True

class NoMoreThanRequiredActivities(Filter):

    is_blocking = True

    def _filter(self):
        return len(self.activities) <= self.required_activities

class OneTeacherPerClass(Filter):

    is_blocking = True

    def _filter(self):
        for classroom, act in self.group_by_class:
            if len(set([a.teacher for a in act])) > 1:
                return False
        return True